import React, { Component } from "react";
import { MapContext } from "./Mapcontext";
import Mapcontainer from "./Mapcontainer";
import "mapbox-gl/dist/mapbox-gl.css";
import Toolbar from "./Toolbar";

class App extends Component {
  render() {
    return (
      <div style={{ width: "100%", height: "100vh", display: "flex" }}>
        <div style={{ width: 88 }}>
          <MapContext.Consumer>
            {({ toggleVisibleDS }) => (
              <Toolbar toggleVisibleDS={toggleVisibleDS} />
            )}
          </MapContext.Consumer>
        </div>
        <MapContext.Consumer>
          {({ initialMap, map, initialDataset }) => (
            <Mapcontainer
              map={map}
              initialMap={initialMap}
              initialDataset={initialDataset}
            />
          )}
        </MapContext.Consumer>
      </div>
    );
  }
}

export default App;
