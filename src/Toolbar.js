import React, { Component } from "react";
import {
  Card,
  CardContent,
  CardActions,
  CardHeader,
  Fab
} from "@material-ui/core";
import {
  Memory,
  Navigation,
  Visibility,
  VisibilityOff
} from "@material-ui/icons";

export default class Toolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleVisible: false
    };
  }
  render() {
    return (
      <Card style={{ height: "100%" }}>
        <CardHeader
          title={
            <Fab>
              <Memory />
            </Fab>
          }
        />
        <CardContent
          style={{
            height: "calc(100vh - 192px)",
            display: "flex",
            justifyContent: "center",
            flexDirection: "column"
          }}
        >
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Fab
              size="small"
              onClick={e => {
                this.setState({
                  toggleVisible: !this.state.toggleVisible
                });
                this.props.toggleVisibleDS("polygon");
              }}
            >
              {this.state.toggleVisible ? <VisibilityOff /> : <Visibility />}
            </Fab>
          </div>
        </CardContent>
        <CardActions style={{ justifyContent: "center" }}>
          <Fab>
            <Navigation />
          </Fab>
        </CardActions>
      </Card>
    );
  }
}
