import React, { Component, createContext } from "react";
import { Map } from "mapbox-gl";
import Mapstyle from "./Mapstyle";

export const MapContext = createContext();

export class Mapprovider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      map: null
    };
  }

  initialMap() {
    this.setState({
      map: new Map({
        container: "map-div",
        style: Mapstyle,
        zoom: 6,
        center: [100.538233, 13.764911]
      })
    });
  }

  initialDataset() {
    const { map } = this.state;
    map.addSource("dataset74", {
      type: "vector",
      tiles: [
        "https://api.vallarismaps.com/v2/tile/74/{z}/{x}/{y}?key=k-cf591b9f-9ebf-482e-b56a-cafccac5ded8"
      ],
      minZoom: 0,
      maxZoom: 14
    });

    map.addLayer({
      id: "polygon",
      filter: ["all", ["==", "$type", "Polygon"]],
      source: "dataset74",
      "source-layer": "74",
      type: "fill",
      paint: {
        "fill-color": "rgba(5, 240, 54, 0.39)"
      }
    });
  }

  toggleVisibleDS(id) {
    const { map } = this.state;
    const style = map.getStyle();
    const layer = style.layers.filter(layer => layer.id === id);
    if (layer.length > 0) {
      map.removeLayer(id);
    } else {
      map.addLayer({
        id: "polygon",
        filter: ["all", ["==", "$type", "Polygon"]],
        source: "dataset74",
        "source-layer": "74",
        type: "fill",
        paint: {
          "fill-color": "rgba(5, 240, 54, 0.39)"
        }
      });
    }
  }

  render() {
    return (
      <MapContext.Provider
        value={{
          ...this.state,
          initialMap: this.initialMap.bind(this),
          initialDataset: this.initialDataset.bind(this),
          toggleVisibleDS: this.toggleVisibleDS.bind(this)
        }}
      >
        {this.props.children}
      </MapContext.Provider>
    );
  }
}
