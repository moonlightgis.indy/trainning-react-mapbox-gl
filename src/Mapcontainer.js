import React, { Component } from "react";

export default class Mapcontainer extends Component {
  async componentDidMount() {
    await this.props.initialMap();
    const waiting = async () => {
      if (!this.props.map.isStyleLoaded()) {
        setTimeout(waiting, 200);
      } else {
        await this.props.initialDataset();
        console.log(this.props.map.getStyle());
      }
    };
    waiting();
  }

  render() {
    return (
      <div
        id="map-div"
        style={{ width: "calc(100vw - 88px)", background: "#2b2929" }}
      />
    );
  }
}
